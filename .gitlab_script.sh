#!/bin/sh

s="src/Alchemy/"
gen() { cat "${s}skyrim.js" "${s}gen.js"; }

compile() {
    curl \
        --data-urlencode "js_code@-" \
        --data-urlencode "js_externs@$1" \
        -d "compilation_level=ADVANCED_OPTIMIZATIONS" \
        -d "output_format=text" \
        -d "output_info=compiled_code" \
        https://closure-compiler.appspot.com/compile
}

d="public/Alchemy/skyrim.js"
store() { tee "$d" | gzip -9 >"${d}.gz"; }

gen | store
# { gen | compile "${s}js_externs.js" | store && [ -s "$d" ]; } || { gen | store; }
