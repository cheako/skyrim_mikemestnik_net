// Code (c) Jimmy Ruska
function add_filter() {
    var a = !!parseInt($("#filter").val(), 10),
        b = parseInt($("#effects").val(), 10);
    __filters.push([
        (a ? "Has " : "Does not have ") + __rel_effect[b],
        (c) => c.includes(b) !== a,
    ]);
    return !1;
}

function add_item_filter(a, b) {
    __filters.push([
        (b ? "Has " : "Does not have ") + __rel_ingredient[a],
        (_, d) => d.includes(a) !== b,
    ]);
    return refresh(!1);
}

var __delete = "delete.png",
    __sadd = "script_add.png",
    __sdelete = "script_delete.png",
    __first_run = !0,
    __filters = [],
    __matches = [],
    __current = 0,
    __have_plus = JSON.parse(localStorage.getItem("a")),
    __have = __have_plus
        ? "have" in __have_plus
            ? [{ name: 0, ingredients: new Set(__have_plus.have) }]
            : "have001" in __have_plus
            ? __have_plus.have001.map((have) => {
                  return {
                      name: have.name,
                      ingredients: new Set(have.ingredients),
                  };
              })
            : [{ name: 0, ingredients: new Set() }]
        : [{ name: 0, ingredients: new Set() }],
    __could_have = __have_plus ? new Set(__have_plus.could_have) : new Set(),
    __effects = [
        ["Cure Disease", 21, 1],
        ["Damage Health", 3, 0],
        ["Damage Magicka", 52, 0],
        ["Damage Magicka Regen", 265, 0],
        ["Damage Stamina", 43, 0],
        ["Damage Stamina Regen", 159, 0],
        ["Fear", 120, 0],
        ["Fortify Alteration", 47, 1],
        ["Fortify Barter", 48, 1],
        ["Fortify Block", 118, 1],
        ["Fortify Carry Weight", 208, 1],
        ["Fortify Conjuration", 75, 1],
        ["Fortify Destruction", 151, 1],
        ["Fortify Enchanting", 14, 1],
        ["Fortify Health", 82, 1],
        ["Fortify Heavy Armor", 55, 1],
        ["Fortify Illusion", 94, 1],
        ["Fortify Light Armor", 55, 1],
        ["Fortify Lockpicking", 25, 1],
        ["Fortify Magicka", 71, 1],
        ["Fortify Marksman", 118, 1],
        ["Fortify One Handed", 118, 1],
        ["Fortify Pickpocket", 118, 1],
        ["Fortify Restoration", 118, 1],
        ["Fortify Smithing", 82, 1],
        ["Fortify Sneak", 118, 1],
        ["Fortify Stamina", 71, 1],
        ["Fortify Two Handed", 118, 1],
        ["Frenzy", 107, 0],
        ["Invisibility", 261, 1],
        ["Lingering Damage Health", 86, 0],
        ["Lingering Damage Magicka", 71, 0],
        ["Lingering Damage Stamina", 12, 0],
        ["Paralysis", 285, 0],
        ["Ravage Health", 6, 0],
        ["Ravage Magicka", 15, 0],
        ["Ravage Stamina", 24, 0],
        ["Regenerate Health", 177, 1],
        ["Regenerate Magicka", 177, 1],
        ["Regenerate Stamina", 177, 1],
        ["Resist Fire", 86, 1],
        ["Resist Frost", 86, 1],
        ["Resist Magic", 51, 1],
        ["Resist Poison", 118, 1],
        ["Resist Shock", 86, 1],
        ["Restore Health", 21, 1],
        ["Restore Magicka", 25, 1],
        ["Restore Stamina", 25, 1],
        ["Slow", 247, 0],
        ["Waterbreathing", 100, 1],
        ["Weakness to Fire", 48, 0],
        ["Weakness to Frost", 40, 0],
        ["Weakness to Magic", 51, 0],
        ["Weakness to Poison", 51, 0],
        ["Weakness to Shock", 56, 0],
    ],
    __all = [
        ["abecean longfin", [51, 25, 53, 23]],
        ["bear claws", [47, 14, 21, 3]],
        ["bee", [47, 36, 39, 54]],
        ["beehive husk", [43, 17, 25, 12]],
        ["bleeding crown", [50, 9, 53, 42]],
        ["blisterwort", [4, 28, 45, 24]],
        ["blue butterfly wing", [4, 11, 3, 13]],
        ["blue dartwing", [44, 22, 45, 6]],
        ["blue mountain flower", [45, 11, 14, 3]],
        ["bone meal", [4, 40, 11, 36]],
        ["briar heart", [46, 9, 33, 19]],
        ["butterfly wing", [45, 8, 32, 2]],
        ["canis root", [4, 21, 20, 33]],
        ["charred skeever hide", [47, 0, 43, 45]],
        ["chaurus eggs", [53, 26, 2, 29]],
        ["chicken's egg", [42, 3, 49, 32]],
        ["creep cluster", [46, 5, 10, 52]],
        ["crimson nirnroot", [1, 4, 29, 42]],
        ["cyrodilic spadetail", [4, 23, 6, 34]],
        ["daedra heart", [45, 5, 2, 6]],
        ["deathbell", [1, 36, 48, 53]],
        ["dragon's tongue", [40, 8, 16, 27]],
        ["dwarven oil", [52, 16, 38, 46]],
        ["ectoplasm", [46, 12, 19, 1]],
        ["elves ear", [46, 20, 51, 40]],
        ["eye of sabre cat", [47, 34, 2, 45]],
        ["falmer ear", [1, 28, 43, 18]],
        ["fire salts", [51, 40, 46, 38]],
        ["fly amanita", [40, 27, 28, 39]],
        ["frost mirriam", [41, 25, 35, 5]],
        ["frost salts", [50, 41, 46, 11]],
        ["garlic", [43, 26, 38, 37]],
        ["giant lichen", [54, 34, 53, 46]],
        ["giant's toe", [4, 14, 10, 5]],
        ["glow dust", [2, 3, 12, 44]],
        ["glowing mushroom", [44, 12, 24, 14]],
        ["grass pod", [43, 35, 7, 46]],
        ["hagraven claw", [42, 31, 13, 8]],
        ["hagraven feathers", [2, 11, 28, 54]],
        ["hanging moss", [2, 14, 3, 21]],
        ["hawk beak", [47, 41, 10, 44]],
        ["hawk feathers", [0, 17, 21, 25]],
        ["histcarp", [47, 19, 5, 49]],
        ["honeycomb", [47, 9, 17, 36]],
        ["human flesh", [1, 33, 46, 25]],
        ["human heart", [1, 2, 3, 28]],
        ["ice wraith teeth", [51, 15, 29, 50]],
        ["imp stool", [1, 30, 33, 45]],
        ["jazbay grapes", [52, 19, 38, 34]],
        ["juniper berries", [50, 20, 37, 5]],
        ["large antlers", [47, 26, 48, 5]],
        ["lavender", [42, 26, 35, 11]],
        ["luna moth wing", [2, 17, 37, 29]],
        ["moon sugar", [50, 41, 46, 38]],
        ["mora tapinella", [46, 30, 39, 16]],
        ["mudcrab chitin", [47, 0, 43, 40]],
        ["namira's rot", [2, 18, 6, 37]],
        ["nightshade", [1, 3, 32, 12]],
        ["nirnroot", [1, 4, 29, 42]],
        ["nordic barnacle", [2, 49, 37, 22]],
        ["orange dartwing", [47, 35, 22, 30]],
        ["pearl", [47, 9, 46, 44]],
        ["pine thrush egg", [47, 18, 53, 44]],
        ["powdered mammoth tusk", [47, 25, 50, 6]],
        ["purple mountain flower", [47, 25, 31, 41]],
        ["red mountain flower", [46, 35, 19, 1]],
        ["river betty", [1, 7, 48, 10]],
        ["rock warbler egg", [45, 21, 4, 52]],
        ["sabre cat tooth", [47, 15, 24, 53]],
        ["salt pile", [52, 23, 48, 38]],
        ["scaly pholiota", [52, 16, 39, 10]],
        ["silverside perch", [47, 5, 34, 41]],
        ["skeever tail", [5, 34, 1, 17]],
        ["slaughterfish egg", [43, 22, 30, 26]],
        ["slaughterfish scales", [41, 30, 15, 9]],
        ["small antlers", [53, 23, 32, 1]],
        ["small pearl", [47, 21, 23, 41]],
        ["snowberries", [40, 13, 41, 44]],
        ["spider egg", [4, 3, 18, 20]],
        ["spriggan sap", [3, 13, 24, 7]],
        ["swamp fungal pod", [44, 31, 33, 45]],
        ["taproot", [52, 16, 38, 46]],
        ["thistle branch", [41, 36, 43, 15]],
        ["torchbug thorax", [47, 31, 52, 26]],
        ["troll fat", [43, 27, 28, 1]],
        ["tundra cotton", [42, 19, 9, 8]],
        ["vampire dust", [29, 46, 37, 0]],
        ["void salts", [54, 42, 1, 19]],
        ["wheat", [45, 14, 5, 31]],
        ["white cap", [51, 15, 46, 35]],
        ["wisp wrappings", [47, 12, 10, 42]],
        ["berit's ashes", [4, 40, 11, 36]],
        ["jarrin root", [1, 2, 4, 3]],
    ],
    __rel_ingredient = __all.map((d) => d[0]),
    __rel_effect_list = __all.map((d) => d[1].sort()), // Are we lazy?
    __rel_effect = __effects.map((d) => d[0]),
    __rel_worth = __effects.map((d) => d[1]),
    __rel_affinity = __effects.map((d) => d[2]),
    __dont_have = new Set(__all.map((_, i) => String(i)));
__have.forEach((a) => a.ingredients.forEach((a) => __dont_have.delete(a)));
$(document).ready(() => {
    $("#add_ingredient").change(add).focus();
    $(":radio").change(() => !1);
    $("#three:checkbox").change(() => !0);
    $.each($(":button"), (a, b) => $(b).button());
    build_effects();
    if (__have_plus) setTimeout(() => refresh(true), 20);
});

function build_effects() {
    var a = [...__effects.entries()].map((a) => {
        return {
            b: a[0],
            name: a[1][0],
            price: a[1][1],
            color: 1 === a[1][2] ? "green" : "red",
        };
    });
    a.sort((a, b) => b.price - a.price);
    $("#listeffects").html(
        a
            .map(
                (a) =>
                    `<span class='effect' data-id='${a.b}' style='font-weight:bold;color:${a.color}'>${a.name} ($${a.price})</span><br/>`
            )
            .join("")
    );
    $(".effect").tooltip({
        bodyHandler: hover_effect,
        delay: 200,
    });
}

function hover_effect() {
    for (
        var a = parseInt($(this).attr("data-id"), 10),
            b =
                "<b>Effect Worth:</b> " +
                __rel_worth[a] +
                "<br/><b>Ingredients With Effect:</b><br/>",
            c = [],
            d = 0,
            f = __all.length;
        d < f;
        d++
    )
        __rel_effect_list[d].includes(a) && c.push(__rel_ingredient[d]);
    return (b += c.sort().join("<br/>"));
}

function key_find(a, b, c) {
    return c.findIndex((d) => b === d[a]);
}

function find(a, b) {
    return b.findIndex((c) => a === c);
}

function add() {
    var a = String($("#add_ingredient").val());
    if (a == "-1") {
        return !1;
    }
    __could_have.delete(a);
    __have.forEach((b) => b.ingredients.delete(a)); // Move this ingredient.
    __have[__current].ingredients.add(a);
    __dont_have.delete(a);
    setTimeout(() => refresh(true), 20);
    return !1;
}

function arr2str(a) {
    `[${a.join(",")}]`;
}

function add_shopping_list() {
    var a = parseInt($("#min_profit").val(), 10),
        b = new Set(__have.map((a) => [...a.ingredients]).flat()),
        z = new Set(
            __all
                .map((_, i) => String(i))
                .filter(
                    (i) =>
                        !(
                            __have.some((a) => a.ingredients.has(i)) ||
                            __could_have.has(i)
                        )
                )
        ),
        c = $("#three").prop("checked"),
        d = new Set([...b]);
    for (const k of b) {
        d.delete(k);
        let m = __rel_effect_list[k];
        for (const n of [...z]) {
            let g = __rel_effect_list[Number(n)],
                q = intersect(m, g);
            if (0 < q.length) {
                let l = [k, Number(n)];
                let w = worth(q, l);
                if (w > a) {
                    __could_have.add(n);
                    z.delete(n);
                }
            }
            if (c) {
                let h = new Set(z),
                    s = __rel_effect_list[Number(n)];
                h.delete(n);
                for (const r of [...h]) {
                    let o = __rel_effect_list[Number(r)],
                        p = intersect(m, s, o),
                        x = intersect(s, o),
                        b = intersect(m, o);
                    if (
                        p.length > q.length &&
                        p.length > x.length &&
                        p.length > b.length
                    ) {
                        let i = [k, Number(n), Number(r)],
                            w = worth(p, i);
                        if (w > a) {
                            __could_have.add(r);
                            z.delete(r);
                            if (z.has(n)) {
                                __could_have.add(n);
                                z.delete(n);
                            }
                        }
                    }
                }
            }
        }
    }
}

function find_matches() {
    var b = [],
        c = $("#three").prop("checked"),
        d = [
            ...new Set([
                ...__have.map((a) => [...a.ingredients]).flat(),
                ...__could_have,
            ]),
        ];
    for (const k of [...d]) {
        d.splice(0, 1);
        let e = [...d],
            m = __rel_effect_list[Number(k)];
        for (const n of [...d]) {
            e.splice(0, 1);
            var g = __rel_effect_list[n],
                q = intersect(m, g);
            if (0 < q.length) {
                let key = [Number(k), Number(n)];
                b.push([key, q, worth(q, key)]);
            }
            if (c)
                for (const i of e.slice(1)) {
                    let o = __rel_effect_list[Number(i)],
                        p = intersect(m, g, o);
                    if (
                        p.length > q.length &&
                        p.length > intersect(m, o).length &&
                        p.length > intersect(g, o).length
                    ) {
                        let key = [Number(k), Number(n), Number(i)];
                        b.push([key, p, worth(p, key)]);
                    }
                }
        }
    }
    b.sort((a, b) => b[2] - a[2]);
    return b;
}

function worth(a, b) {
    var c = a.reduce(
        (accumulator, currentValue) => accumulator + __rel_worth[currentValue],
        0
    );
    b.includes(33) && (c += multiplier(14, 6, 82, a));
    b.includes(17) && (c += multiplier(4, 906 / 269, 43, a));
    if (a.includes(1))
        for (d of [
            [92, 193.67],
            [66, 791 / 23],
            [58, 291 / 23],
            [17, 76 / 23],
            [20, 35 / 23],
        ]) {
            var h = multiplier(d[0], d[1], 3, b);
            if (0 < h) {
                c += h;
                break;
            }
        }
    return c;
}

function multiplier(a, b, c, d) {
    return undefined !== find(a, d) ? Math.ceil(c * b) - c : 0;
}

function refresh(a) {
    __first_run &&
        ($("#listeffects").css("visibility", "visible"),
        $("#vid").remove(),
        (__first_run = !1));
    $("#results").html("");
    $("#warn").html(
        "Generating all possible recipes, this may take a few seconds...<br/><br/>"
    );
    setTimeout((a) => refresh1(a), 20, a.toString());
    return !1;
}

function hover_ingredients() {
    var a = parseInt($(this).attr("data-name"), 10);
    if (1e3 === a) return "";
    for (
        g = __rel_effect_list[a], b = "", c = 0, d = 0, f = g.length;
        d < f;
        d++
    )
        var h = __effects[a[d]],
            k = h[0],
            e = h[1],
            h = h[2],
            c = c + e,
            b =
                b +
                ("<span style='font-weight:bold;color:" +
                    (1 === h ? "green" : "red") +
                    "'>" +
                    k +
                    " ($" +
                    e +
                    ")</span><br/>");
    return (b =
        b +
        ("<br/><b>Combined Worth:</b> $" + c + "<br/>") +
        ("<b>Average Worth:</b> $" + Math.round(c / 4)));
}

function refresh1(g) {
    var a = $("<tbody></tbody>"),
        b = $("#pure").prop("checked"),
        c = $("#positive").prop("checked"),
        d = $("#negative").prop("checked"),
        x = $("<div></div>"),
        f = $("#three").prop("checked"),
        h = parseInt($("#max_results").val(), 10),
        k = $("<div></div>"),
        l = $("<ul></ul>"),
        n = __current,
        q = [],
        t = $("<table></table>"),
        y = parseInt($("#min_profit").val(), 10),
        z = $("<div></div>");
    localStorage.setItem(
        "a",
        JSON.stringify({
            have001: __have.map((have) => {
                return {
                    name: have.name,
                    ingredients: [...have.ingredients],
                };
            }),
            could_have: [...__could_have],
        })
    );
    $("#add_ingredient").html([
        new Option("Add Ingredient", -1),
        ...[...__dont_have]
            .reduce((acc, v) => {
                let name = __all[Number(v)][0],
                    first_letter = name[0].toUpperCase(),
                    last = acc.slice(-1)[0],
                    option = new Option(
                        name
                            .replace(/^./, (a) => a.toUpperCase())
                            .replace(
                                /(\s+)\S/g,
                                (a, b) => `${b}${a.substr(-1).toUpperCase()}`
                            ),
                        v
                    );
                if (typeof last === "object" && first_letter == last.i) {
                    last.a.push(option);
                } else {
                    acc.push({ i: first_letter, a: [option] });
                }
                return acc;
            }, [])
            .map((v) => {
                if (v.a.length < 3) {
                    return $(v.a);
                } else {
                    return [
                        $("<optgroup></optgroup>")
                            .attr("label", v.i.toUpperCase())
                            .html(v.a),
                    ];
                }
            })
            .flat(),
    ]);
    k.attr("id", "ingredients")
        .css("border", "1px solid #ccc")
        .css("padding-left", "5px")
        .css("width", 280)
        .html([
            $("<h4></h4>")
                .css("margin", 0)
                .css("padding", 0)
                .html("Your Ingredients: Icon Legend"),
            $("<br/>"),
            $("<img></img>").attr("src", __delete),
            " = I don't have this ",
            $("<br/>"),
            $("<img></img>").attr("src", __sadd),
            " = Show recipes that have this",
            $("<br/>"),
            $("<img></img>").attr("src", __sdelete),
            " = Exclude recipes that have this",
            z,
        ]);
    z.attr("id", "ingredient_tabs")
        .css("line-height", "40px")
        .css("white-space", "nowrap");
    $("#ingredients .ingredient").tooltip({
        bodyHandler: hover_ingredients,
        delay: 200,
    });
    if (
        __have.every((l) => 0 === l.ingredients.size) &&
        0 === __could_have.size
    )
        return (
            (document.getElementById("results").innerHTML =
                "<br/>No ingredients selected."),
            $("#controls").css("visibility", "hidden"),
            $("#warn").html(""),
            !1
        );
    g && (__matches = find_matches());
    if (0 === __matches.length)
        return (
            (document.getElementById("results").innerHTML =
                "<br/>No recipes found. Add more ingredients."),
            $("#controls").css("visibility", "hidden"),
            $("#warn").html(""),
            !1
        );
    x.html(
        [
            ["<br/>"],
            0 === __filters.length
                ? ["<br/>"]
                : __filters
                      .map((_, b) => [
                          $("<a></a>")
                              .attr("href", "#")
                              .click(b, (b) => {
                                  __filters.splice(b.data, 1);
                                  return refresh(true);
                              })
                              .html($("<img></img>").attr("src", __delete)),
                          ` ${__filters[b][0]}`,
                      ])
                      .flat(),
            ["<br/>", t],
        ].flat()
    );
    t.attr("id", "potiontable")
        .attr("cellpadding", 3)
        .attr("cellspacing", 0)
        .attr("border", 1)
        .html([
            "<thead><tr><th>Profit</th><th>Ingredient 1</th><th>Ingredient 2</th><th>Ingredient 3</th><th>Effects</th></tr></thead>",
            a,
        ]);
    {
        let k = 0;
        for (const j of __matches) {
            let g = j[0],
                n = j[1],
                is_low_price = j[2] <= y,
                aa = `$${j[2]}`;
            if (
                !(0 < __filters.length && filter(__filters, n, g)) &&
                (3 !== g.length || f)
            ) {
                let l = "";
                for (i = null, o = 0, p = n.length; o < p; o++) {
                    var r = __effects[n[o]];
                    null === i
                        ? (i = r[2])
                        : !1 !== i && i !== r[2] && (i = !1);
                    l += `<span class='effect' data-id='${
                        n[o]
                    }' style='font-weight:bold;color:${
                        1 === r[2] ? "green" : "red"
                    }'>${r[0]}</span><br/>`;
                }
                if (!(b && !1 === i) && !(c && 1 !== i) && !(d && 0 !== i)) {
                    if (h === k++) {
                        a.append(
                            $("<tr></tr>").html(
                                $("<td></td>")
                                    .attr("colspan", 5)
                                    .text(
                                        `Limiting to ${h} table rows. Please add filters to avoid slow rendering.`
                                    )
                            )
                        );
                        break;
                    }
                    g.forEach((element) => {
                        if (!(element in q)) q[element] = new Set();
                        q[element].add(aa);
                    });
                    let s = g.map((r) =>
                        $("<span></span>")
                            .attr("data-name", r)
                            .addClass("ingredient")
                            .addClass(() =>
                                __could_have.has(String(r))
                                    ? "shopping-list"
                                    : ""
                            )
                            .html(fixup_ingredient(__rel_ingredient[r]))
                    );
                    let ingredient_options = (a) =>
                            void 0 === a
                                ? ""
                                : [
                                      "<br/>",
                                      $("<a href='#'></a>")
                                          .click(a, (a) => remove_item(a.data))
                                          .html(
                                              $("<img></img>").attr(
                                                  "src",
                                                  __delete
                                              )
                                          ),
                                      "&nbsp;&nbsp; ",
                                      $("<a href='#'></a>")
                                          .click(a, (a) =>
                                              add_item_filter(a.data, true)
                                          )
                                          .html(
                                              $("<img></img>").attr(
                                                  "src",
                                                  __sadd
                                              )
                                          ),
                                      "&nbsp;&nbsp; ",
                                      $("<a href='#'></a>")
                                          .click(a, (a) =>
                                              add_item_filter(a.data, false)
                                          )
                                          .html(
                                              $("<img></img>").attr(
                                                  "src",
                                                  __sdelete
                                              )
                                          ),
                                  ],
                        ab = (q) => {
                            let a = $("<td></td>"),
                                b = $("<span></span>");
                            a.html(b);
                            if (1 < q.size) {
                                b.html([
                                    [...q].slice(0, -1).slice(0, 3).join(","),
                                    4 < q.size ? `...${q.size - 4}` : "",
                                ]);
                                a.prepend("<br/>");
                            }
                            return a;
                        },
                        ac = (q, s, g) =>
                            ab(q).prepend(ingredient_options(g)).prepend(s),
                        ad = $("<tr></tr>");
                    a.append(ad);
                    ad.html([
                        $("<td></td>")
                            .addClass(is_low_price ? "low-price" : undefined)
                            .html(aa),
                        ac(q[g[0]], s[0], g[0]),
                        ac(q[g[1]], s[1], g[1]),
                        3 === g.length
                            ? ac(q[g[2]], s[2], g[2])
                            : $("<td></td>").html(
                                  $("<span></span>")
                                      .attr("data-name", 1e3)
                                      .attr("class", "ingredient")
                                      .html("none")
                              ),
                        $("<td></td>").html(l),
                    ]);
                }
            }
        }
    }
    z.html(l);
    if (0 !== __could_have.size) {
        n++;
        l.html(
            $("<li></li>").html([
                $(
                    '<a href="#ingredients-shopping-list"><span>Shopping List</span></a>'
                ).click(() => (__current = 0 || !0)),
                $("<a></a>")
                    .attr("href", "#")
                    .click(() => {
                        __could_have = new Set();
                        return refresh(true);
                    })
                    .html($("<img></img>").attr("src", __delete)),
            ])
        );
        let j = $('<div id="ingredients-shopping-list"></div>');
        for (const p of __could_have) {
            let i = $("<span></span>");
            i.addClass("ingredient")
                .attr("data-name", p)
                .html(
                    `${fixup_ingredient(__rel_ingredient[Number(p)])}${
                        Number(p) in q ? `(${[...q[Number(p)]][0]})` : ""
                    }`
                );
            j.append([
                $("<a></a>")
                    .attr("href", "#")
                    .click(p, (p) => remove_item(Number(p.data)))
                    .html($("<img></img>").attr("src", __delete)),
                $("<a></a>")
                    .attr("href", "#")
                    .click(p, (p) => add_item_filter(Number(p.data), true))
                    .html($("<img></img>").attr("src", __sadd)),
                $("<a></a>")
                    .attr("href", "#")
                    .click(p, (p) => add_item_filter(Number(p.data), false))
                    .html($("<img></img>").attr("src", __sdelete)),
                "&nbsp;&nbsp;",
                i,
                "<br/>",
            ]);
        }
        z.append(j);
    }
    __have.forEach((e, i) => {
        l.append(
            $("<li></li>")
                .addClass("ui-closable-tab")
                .append([
                    $("<a></a>")
                        .attr("href", `#ingredients-${i}`)
                        .click(i, (i) => (__current = i.data || !0))
                        .html(
                            $("<input></input>")
                                .attr("type", "text")
                                .val(e.name)
                                .change(
                                    e,
                                    (e) =>
                                        (e.data.name = $(e.target).val() || !0)
                                )
                        ),
                    $("<a></a>")
                        .attr("href", "#")
                        .click(i, (i) => {
                            __have.splice(i.data, 1);
                            return refresh(true);
                        })
                        .html($("<img></img>").attr("src", __delete)),
                ])
        );
        let j = $(`<div id="ingredients-${i}"></div>`),
            m = [...e.ingredients];
        m.sort((a, b) => a - b);
        for (const p of m) {
            let i = $("<span></span>");
            i.addClass("ingredient")
                .attr("data-name", p)
                .html(
                    `${fixup_ingredient(__rel_ingredient[Number(p)])}${
                        Number(p) in q ? `(${[...q[Number(p)]][0]})` : ""
                    }`
                );
            j.append([
                $("<a></a>")
                    .attr("href", "#")
                    .click(p, (p) => remove_item(Number(p.data)))
                    .html($("<img></img>").attr("src", __delete)),
                $("<a></a>")
                    .attr("href", "#")
                    .click(p, (p) => add_item_filter(Number(p.data), true))
                    .html($("<img></img>").attr("src", __sadd)),
                $("<a></a>")
                    .attr("href", "#")
                    .click(p, (p) => add_item_filter(Number(p.data), false))
                    .html($("<img></img>").attr("src", __sdelete)),
                "&nbsp;&nbsp;",
                i,
                "<br/>",
            ]);
        }
        z.append(j);
    });
    var e = () => {
        l.append(
            $("<li></li>").append(
                $(`<a href="#ingredients-${__have.length}">New</a>`).click(
                    { a: __have.length, z: z },
                    (a) => {
                        __current = a.data.a;
                        __have[__current] = {
                            name: __current,
                            ingredients: new Set(),
                        };
                        l.children()
                            .last()
                            .addClass("ui-closable-tab")
                            .html([
                                $("<a></a>")
                                    .attr("href", `#ingredients-${__current}`)
                                    .append(
                                        $("<input></input>")
                                            .attr("type", "text")
                                            .val(__current)
                                            .change(
                                                __current,
                                                (i) =>
                                                    (__have[i.data].name =
                                                        $(i.target).val() || !0)
                                            )
                                    ),
                                $("<a></a>")
                                    .attr("href", "#")
                                    .click(__current, (i) => {
                                        __have.splice(i.data, 1);
                                        return refresh(true);
                                    })
                                    .html(
                                        $("<img></img>").attr("src", __delete)
                                    ),
                            ]);
                        e();
                        z.append(
                            $(`<div id="ingredients-${__current}"></div>`)
                        );
                        a.data.z.tabs("refresh");
                        return false;
                    }
                )
            )
        );
    };
    e();
    $("#added").html(k);
    z.tabs({
        active: n,
    });
    $("#results").html([x, "<br/><br/><br/>"]);
    $("#warn").html("");
    $("#controls").css("visibility", "visible");
    setTimeout(add_hover_effects, 20);
    return !1;
}

function remove_item(a) {
    __have.forEach((b) => b.ingredients.delete(String(a)));
    __could_have.delete(String(a));
    __dont_have.add(String(a));
    for (const [b, k] of __matches.entries())
        if (k[0].includes(a)) __matches.splice(b, 1);
    return refresh(!1);
}

function filter(a, b, c) {
    for (var d = 0, f = a.length; d < f; d++)
        if (((fun = a[d][1]), fun(b, c))) return !0;
    return !1;
}

function intersect() {
    var a = [...arguments].flat(),
        counts = [];
    for (const item of a)
        if (typeof counts[item] == "undefined") {
            counts[item] = 1;
        } else counts[item] = 2;
    return [...counts.entries()]
        .filter((a) => 1 < Number(a[1]))
        .map((a) => a[0]);
}

function add_hover_effects() {
    $(".effect").tooltip({
        content: hover_effect,
        delay: 200,
    });
    $(".ingredient").tooltip({
        content: hover_ingredients,
        delay: 200,
    });
}

function fixup_ingredient(a) {
    return a
        .replace(/^./, (a) => a.toUpperCase())
        .replace(/\s+\S/g, (a) => `&nbsp;${a.substr(-1).toUpperCase()}`);
}
